package lava.actions;

import etwin.Obfu;
import merlin.IAction;
import merlin.IActionContext;

class LavaAction implements IAction {
    public var name(default, null): String = Obfu.raw("lava");
    public var isVerbose(default, null): Bool = false;

    private var mod: Lava;

    public function new(mod: Lava) {
        this.mod = mod;
    }

    public function run(ctx: IActionContext): Bool {
        return this.mod.isEnabled();
    }
}
