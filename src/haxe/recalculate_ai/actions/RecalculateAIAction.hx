package recalculate_ai.actions;

import etwin.Obfu;
import merlin.IActionContext;
import merlin.IAction;

class RecalculateAIAction implements IAction {
    public var name(default, null): String = Obfu.raw("recalculateAI");
    public var isVerbose(default, null): Bool = false;

    private var mod: RecalculateAI;


    public function new(mod: RecalculateAI) {
        this.mod = mod;
    }

    public function run(ctx: IActionContext): Bool {
        var game: hf.mode.GameMode = ctx.getGame();
        var y = ctx.getOptInt(Obfu.raw("y"));
        var y1 = y.or(_ => ctx.getInt(Obfu.raw("y1")));
        var y2 = y.or(_ => ctx.getInt(Obfu.raw("y2")));

        if (y1 > y2) {
            var tmp = y1;
            y1 = y2;
            y2 = tmp;
        }
        y2++;

        this.mod.recalculateAI(game, y1, y2);

        return false;
    }
}
