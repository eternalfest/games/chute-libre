package lava;
import lava.actions.LavaAction;

import etwin.Obfu;
import patchman.module.Run;
import patchman.IPatch;
import patchman.Ref;
import merlin.IAction;
import etwin.ds.FrozenArray;
import ef.api.run.IRun;
import etwin.ds.WeakMap;
import etwin.flash.MovieClip;

@:build(patchman.Build.di())
class Lava {
    @:diExport
    public var lavaAction(default, null): IAction;
    @:diExport
    public var patches(default, null): FrozenArray<IPatch>;

    public static var OPTION_NAME = Obfu.raw("lava");
    public static var MOCKING_NAME = Obfu.raw("$mocking");
    private var enabled: Bool;

    private static var STARTER(default, null): WeakMap<hf.mode.GameMode, Float> = new WeakMap();
    private static var MC_STARTER(default, null): WeakMap<hf.mode.GameMode, MovieClip> = new WeakMap();

    public function isEnabled(): Bool {
        return this.enabled;
    }

    public function new(runMod: Run) {
        var run: IRun = runMod.getRun();
        this.enabled = (run.gameOptions.indexOf(OPTION_NAME) >= 0);

        this.lavaAction = new LavaAction(this);

        var patches = [];
        if (this.isEnabled()) {
            patches = [
                Ref.auto(hf.mode.GameMode.initGame).after(function(hf: hf.Hf, self: hf.mode.GameMode): Void {
                    hf.Data.EXTRA_LIFE_STEPS = [];
                }),

                Ref.auto(hf.entity.Player.initPlayer).after(function(hf: hf.Hf, self: hf.entity.Player, g: hf.mode.GameMode, x: Float, y: Float): Void {
                    self.lives = 0;
                }),

                Ref.auto(hf.entity.Player.update).after(function(hf: hf.Hf, self: hf.entity.Player) {
                    if (self.fl_stable) {
                        self.fl_shield = false;
                        self.killHit(null);
                        self.game.fxMan.attachFx(self.x, self.y, 'hammer_fx_burning');
                        if (self.game.getDynamicVar(MOCKING_NAME)) {
                            self.game.attachPop(Obfu.raw("HAHAHAHAHAHAHAHAHAHAHAHAHAHAHAHAHAHAHAHAHAHAHAHAHAHAHAHAHAHAHAHAHAHAHAHAHAHAHAHAHAHAHAHAHAHAHAHAHAHAHAHAHAHAHAHAHAHAHAHAHA"), false);
                            self.game.playMusic(4);
                        }
                    }
                    if (self.game.getDynamicVar(MOCKING_NAME) && self.animId == hf.Data.ANIM_PLAYER_DIE.id) {
                        self.game.bulletTime(hf.Data.SECOND * 10);
                    }
                }),

                Ref.auto(hf.mode.GameMode.onLevelReady).after(function(hf: hf.Hf, self: hf.mode.GameMode) {
                    if (self.world.currentId == 0) {
                        self.lock();
                        self.world.lock();
                        STARTER.set(self, hf.Data.SECOND * 3.5);
                        MC_STARTER.set(self, self.depthMan.attach('hammer_interf_starter', hf.Data.DP_INTERF));
                        MC_STARTER.get(self)._x = hf.Data.DOC_WIDTH * 0.5;
                        MC_STARTER.get(self)._y = hf.Data.DOC_HEIGHT * 0.5;
                        untyped {
                            MC_STARTER.get(self).field.text = '';
                        }
                    }
                }),

                Ref.auto(hf.mode.GameMode.main).after(function(hf: hf.Hf, self: hf.mode.GameMode) {
                    if (STARTER.get(self) >= 0)
                    {
                        var v3 = Math.ceil(STARTER.get(self) / 32);
                        STARTER.set(self, STARTER.get(self) - hf.Timer.tmod);
                        var v4 = Math.ceil(STARTER.get(self) / 32);
                        MC_STARTER.get(self)._xscale *= 0.99;
                        MC_STARTER.get(self)._yscale = MC_STARTER.get(self)._xscale;
                        MC_STARTER.get(self)._alpha *= 0.95;
                        if (v3 != v4) {
                            untyped {
                                MC_STARTER.get(self).field.text = '' + v4;
                            }
                            MC_STARTER.get(self)._alpha = 100;
                            MC_STARTER.get(self)._xscale = 70 + 120 - v4 * 40;
                            MC_STARTER.get(self)._yscale = MC_STARTER.get(self)._xscale;
                        }
                    }

                    if (self.fl_lock && STARTER.get(self) < 0)
                    {
                        MC_STARTER.get(self).removeMovieClip();
                        self.fxMan.attachAlert(hf.Lang.get(100));
                        STARTER.set(self, 99999999);
                        self.unlock();
                        self.world.unlock();
                    }
                }),
            ];
        }
        this.patches = FrozenArray.from(patches);
    }
}
