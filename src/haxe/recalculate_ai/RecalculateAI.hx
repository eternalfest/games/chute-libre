package recalculate_ai;
import patchman.DebugConsole;
import recalculate_ai.actions.RecalculateAIAction;

import etwin.ds.Nil;
import etwin.ds.FrozenArray;
import patchman.IPatch;
import patchman.Ref;
import merlin.IAction;


@:build(patchman.Build.di())
class RecalculateAI {
    @:diExport
    public var recalculateAIAction(default, null): IAction;
    @:diExport
    public var patches(default, null): FrozenArray<IPatch>;

    public var disable: Bool = false;

    public function new() {
        this.disable = false;

        var recalculateAI = new RecalculateAIAction(this);

        var patches = [
            Ref.auto(hf.GameManager.progress).prefix(function(hf: hf.Hf, self: hf.GameManager, ratio: Float) {
                return this.disable ? Nil.some(null) : Nil.none();
            }),

            Ref.auto(hf.levels.GameMechanics.onParseIAComplete).prefix(function(hf: hf.Hf, self: hf.levels.GameMechanics) {
                return this.disable ? Nil.some(null) : Nil.none();
            })
        ];

        this.recalculateAIAction = recalculateAI;
        this.patches = FrozenArray.from(patches);
    }

    public function recalculateAI(game: hf.mode.GameMode, y1: Int, y2:Int) {
        var old = game.root.Data.MAX_ITERATION;
        game.root.Data.MAX_ITERATION = (y2 - y1) * game.root.Data.LEVEL_WIDTH;

        this.disable = true;

        for (y in y1...y2) {
            for (x in 0...game.root.Data.LEVEL_WIDTH) {
                game.world.flagMap[x][y] = 0;
                game.world.fallMap[x][y] = -1;
            }
        }
        game.world.parseCurrentIA({ cx: 0, cy: y1 });

        this.disable = false;

        game.root.Data.MAX_ITERATION = old;
    }
}
