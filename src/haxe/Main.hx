import lava.Lava;
import maxmods.AddLink;
import maxmods.FixedBackground;
import bugfix.Bugfix;
import debug.Debug;
import hf.Hf;
import merlin.Merlin;
import game_params.GameParams;
import patchman.IPatch;
import patchman.Patchman;
import better_script.NoNextLevel;
import recalculate_ai.RecalculateAI;

@:build(patchman.Build.di())
class Main {
  public static function main(): Void {
    Patchman.bootstrap(Main);
  }

  public function new(
      bugfix: Bugfix,
      debug: Debug,
      gameParams: GameParams,
      noNextLevel: NoNextLevel,
      add_link: AddLink,
      fixed_background: FixedBackground,
      recalculate_ai: RecalculateAI,
      lava: Lava,
      merlin: Merlin,
      patches: Array<IPatch>,
      hf: Hf
  ) {
    Patchman.patchAll(patches, hf);
  }
}
